# Organisation

Les points scores sont des présentations courtes (15 à 30 minutes) permettant d'illustrer rapidement un à plusieurs scores aidant à la sélection de variant. L'objectif est de produire une liste de carte d'identité de score, un tableau récapitulatif des scores, la description des corrélations et dépendances entre scores.

Quand : Le mardi matin de 11h à 12h.

Où : salle de réunion génétique somatique des cancers (GSC) avec possibilité de visio (https://meet.jit.si/PointsScores).

Qui : Toute personne intéressée pour aborder des notions de scores.

# Participants list

| Participant | Date | Subjet |
|---|---|---|
| KIM Artem | 16/03/2021 | pext |
| FAOUCHER Marie | 23/03/2021 |  |
| ROLLIER Paul | 30/03/2021 |  |
| LEBEURRIER Manuel | 06/04/2021 |  |
| LOKCHINE Anna | 13/04/2021 |  |
| HAMDI ROZE Houda | 20/04/2021 |  |
| BEAUMONT Marie | 27/04/2021 |  |
| DUBOURG Christele | 04/05/2021 |  |
| ALIOUAT Amyra | 11/05/2021 |  |
| LESPAGNOL Alexandra | 18/05/2021 |  |
| SWANN MEYER | 25/05/2021 |  |

Dispensed :

* DE TAYRAC Marie
* DENOUAL Florent

No cheating :

`MOMOMOTUS = c("FAOUCHER Marie", "HAMDI ROZE Houda", "SWANN MEYER", "BEAUMONT Marie", "ROLLIER Paul", "DUBOURG Christele", "LESPAGNOL Alexandra", "LOKCHINE Anna", "ALIOUAT Amyra", "LEBEURRIER Manuel")`

`set.seed(42)`

`sample(MOMOMOTUS)`

# Subject list

* GERP
* phastCons
* phyloP
* Grantham
* SIFT
* PolyPhen
* MutationTaster
* MutationAssessor
* FATHMM
* LRT
* SiPhy
* VEST
* PROVEAN
* MutPred
* NNSplice
* CADD
* DANN
* REVEL
* MCAP
* MVP
* MPC
* MetaLR/SVM
* Exac (pLI, pRec, lof_z)
* Eigen
* ...

# Score ID card

Description of scores used for genomic interpretation of variants.

Format :
* Name
* Version
* Type = {Score | Metascore}
* Category = {Conservation | Functionnal}
* Scale = {nucleotide | AA | gene | transcript | protein | pathway }
* Thematic = {Constit | Somatic }
* Usefull for = {SVN | Indels | CNV | splicing | coding | non coding | promoters | enhancers | UTRs | expression }
* Short description = {One line to describe the idea bhind the score.}
* Orientation and range = {Orientation, range, authors threshold, related scores/rankscore/predictions}
* Methodology = {Publications material and method}
* VCF description = {VEP description header, dbNSFP description header}
* Long Description = {Many lines to explain in depth the score.}
* Training set = {Training set}
* Testing set = {Testing set}
* Performance = {AUC, SN, SP}
* Exemple = {Alamut screenshot, genome browser screenshot}
* Source = {Online tool, publication}

# Score and metascore : quantum intrication

As some score are based on other scores the following array should ive a nice overview of their interactions:

| Metascores/scores | Number of annotations | [GERP](GERP.md) | [phastCons](phyloP_PhasCons.md) | [phyloP](phyloP_PhasCons.md) | Grantham | [SIFT](SIFT.md) | [PolyPhen](polyphen2.md) | [MutationTaster](MutationTaster.md) | [MutationAssessor](MutationAssessor.md) | [FATHMM](FATHMM.md) | [LRT](LRT.md) | [SiPhy](SiPhy.md) | 1000G AF | ESP AF | ClinVar | HGMD | gnomAD/BRAVO variant density | [VEST](VEST.md) | [PROVEAN](PROVEAN.md) | [MutPred](MutPred.md) | Gene Annotation? | bstatistic | mirSVR | targetScan | chromHMM | Encode expresion | Encode  nucleosome position | Encode  histone modification | Encode open chromatine | Encode DNAse hypersensitiv | Encode promoter-associated regulatory features | JASPAR | Segway | tOverlapMotifs | TFBS | mutationDensity | nearestMutation | dbscSNV | GC | CpG | NNSplice | [CADD](CADD.md) | [MetaLR/SVM](Meta.md) | multiple-sequence alignment | RVIS24 | PAM250 | BLOSUME62 | BioPlex 2.0 Network | CORUM | PrePPI | dbPTM | GPS-SUMO | GPS3.0 | UbiProber | Exac (pLI, pRec, lof_z) | domino | s_het | Eigen | missense baldness |
| - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - | - |
| [DANN](DANN.md) | 63 | X | X | X | X | X |  |  |  |  |  |  | X | X |  |  | X |  |  |  | X | X | X | X | X | X | X | X | X | X |  |  | X | X | X | X | X | X | X | X |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
| [CADD](CADD.md) | 63 | X | X | X | X | X |  |  |  |  |  |  | X | X |  |  | X |  |  |  | X | X | X | X | X | X | X | X | X | X |  |  | X | X | X | X | X | X | X | X |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
| [MetaLR/SVM](Meta.md) | 10 | X |  | X |  | X | X | X | X | X | X | X | X |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
| [REVEL](REVEL.md) | 13 | X | X | X |  | X | X | X | X | X | X | X |  |  |  |  |  | X | X | X |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
| MutationTaster2 |  |  | X | X |  |  |  |  |  |  |  |  | X |  | X | X |  |  |  |  |  |  |  |  |  |  |  | X |  | X | X | X |  |  |  |  |  |  |  |  | X |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
| [VEST](VEST.md) | 2 |  |   |   |  |  |  |  |  |  |  |  |  | X |  | X |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
| [M-CAP](M-CAP.md) | 413 | X | X | X |  | X | X | X | X | X | X | X |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  | X | X | 397 | X | X | X |  |  |  |  |  |  |  |  |  |  |  |  |
| [MVP](MVP.md) |  | X | X | X |  | X | X | X | X | X | X | X |  |  |  |  |  | X | X | X |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  | X |  |  |  |  |  | X | X | X | X | X | X | X | X | X | X | X | X | X | X | | 
| [MPC](MPC.md) | 4 |  |  |  | X |  | X |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  | X |  |  |  |  |  |  |  |  |  |  |  | X |

# Benchmark publications

An array resume of [Performance evaluation of pathogenicity-computation methods for missense variants](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6125674/)

<a href="https://github.com/mlebeur/scores/blob/main/Tools%20summary.pdf">Tools resume</a>

[Source S10](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6125674/)


# Benchmark on real datasets

# Score correlation

Some scores are more or less correlated as we can see on :

* the Meta publication dataset :  

![Meta sores correlation plot](Meta_correlation_plot.png)

* the Eigen publication dataset :  

![Eigen scores correlation plot](Eigen_correlation_plot_scores.png)

* the MVP publication dataset :  

![MVP scores correlation plot](MVP_correlation.png)

<a href="https://github.com/mlebeur/scores/blob/main/Supplementary%20Figure%20S10%20Spearman%20rank%20correlation%20coefficient%20based%20on%20three%20benchmark%20datasets.pdf">Another tool correlation</a>
